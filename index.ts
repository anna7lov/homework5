import { TeachingEmployee } from "./src/abstractClasses/TeachingEmployee";
import { Administrator } from "./src/classes/Administrator";
import { AssociateProfessor } from "./src/classes/AssociateProfessor";
import { Course } from "./src/classes/Course";
import { ElectiveCourse } from "./src/classes/ElectiveCourse";
import { Professor } from "./src/classes/Professor";
import { Rector } from "./src/classes/Rector";
import { Student } from "./src/classes/Student";
import { Ticket } from "./src/classes/Ticket";
import { University } from "./src/classes/University";
import {
    AccreditationStatus,
    CourseName,
    Major,
    YearOfStudy,
    EmployeeRole,
    TicketType
} from "./src/enums/enums";
import { ICourse, IUniversity } from "./src/interfaces/interfaces";
import { Employee } from "./src/types/types";
import { registeredClasses } from "./src/decorators/decorators";

const university1 = new University(
    "Harvard University",
    1636,
    "Cambridge",
    AccreditationStatus.Accredited
);
const university2 = new University(
    "Massachusetts Institute of Technology",
    1861,
    "Cambridge",
    AccreditationStatus.Accredited
);
const university3 = new University(
    "Stanford University",
    1885,
    "Stanford",
    AccreditationStatus.Accredited
);
const university4 = new University(
    "California Institute of Technology",
    1891,
    "Pasadena",
    AccreditationStatus.Accredited
);
const university5 = new University(
    "University of Cambridge",
    1209,
    "Cambridge",
    AccreditationStatus.Accredited
);

console.log(university1.getFormatedLocation());

const rector = new Rector(1, "John Black", 52, "male", "swimming", 6, null);
rector.applyForJob(university1);
rector.salary = rector.calculateSalary();
console.log(rector);

const mathematics = new Course<TeachingEmployee>(CourseName.Mathematics);
const computerScience = new Course<TeachingEmployee>(
    CourseName.ComputerScience
);
const databases = new Course<TeachingEmployee>(CourseName.Databases);
const philosophy = new Course<TeachingEmployee>(CourseName.Philosophy);
const english = new Course<TeachingEmployee>(CourseName.English);
const literature = new Course<TeachingEmployee>(CourseName.Literature);
const finance = new Course<TeachingEmployee>(CourseName.Finance);
const economics = new Course<TeachingEmployee>(CourseName.Economics);

type MajorCourses = Record<Major, ICourse[]>;

const majorCourses: MajorCourses = {
    [Major.ComputerScience]: [mathematics, computerScience, databases],
    [Major.DataScience]: [
        mathematics,
        computerScience,
        philosophy,
        databases,
        finance
    ],
    [Major.ArtificialIntelligence]: [
        mathematics,
        computerScience,
        philosophy,
        english
    ],
    [Major.SoftwareEngineering]: [
        mathematics,
        computerScience,
        philosophy,
        english
    ],
    [Major.Cybersecurity]: [mathematics, computerScience, english],
    [Major.Marketing]: [finance, economics, english],
    [Major.Linguistics]: [philosophy, english, literature]
};

console.log("Courses in Marketing:", majorCourses.Marketing);
console.log("Courses in Data Science:", majorCourses["Data Science"]);

university1.addCourse(mathematics);
university1.addCourse(databases);
university5.addCourse(economics);

const professor1 = new Professor(
    100,
    "Mr. Brown",
    35,
    "male",
    "reading",
    7,
    new Set()
);
professor1.applyForJob(university1);
professor1.addCourseToTeaching(university1, mathematics);
professor1.addCourseToTeaching(university1, databases);
professor1.addCourseToTeaching(university1, economics);
professor1.addCourseToTeaching(university3, economics);
professor1.applyForJob(university4);
professor1.addCourseToTeaching(university4, economics);
professor1.applyForJob(university5);
professor1.addCourseToTeaching(university5, economics);
university1.addCourse(economics);
professor1.addCourseToTeaching(university1, economics);
professor1.salary = professor1.calculateSalary();
console.log(professor1);

const professor2 = new Professor(
    101,
    "Mr. Red",
    25,
    "male",
    "music",
    2,
    new Set()
);
professor2.applyForJob(university1);
professor2.addCourseToTeaching(university1, mathematics);
console.log(professor2);

const professor3 = new Professor(
    102,
    "Selena Dark",
    35,
    "female",
    "music",
    8,
    new Set()
);
professor3.applyForJob(university1);
professor3.addCourseToTeaching(university1, mathematics);
professor3.salary = professor3.calculateSalary();
console.log(professor3);

const associateProfessor1 = new AssociateProfessor(
    200,
    "Mr. Smith",
    27,
    "male",
    "music",
    3,
    new Set()
);
associateProfessor1.applyForJob(university1);

const universitiesSet = new Set<IUniversity>();
universitiesSet.add(university3);
universitiesSet.add(university5);
const associateProfessor2 = new AssociateProfessor(
    201,
    "Alex Portman",
    45,
    "male",
    "reading",
    13,
    universitiesSet
);
associateProfessor2.applyForJob(university1);
associateProfessor2.addCourseToTeaching(university1, databases);
console.log(associateProfessor2);

professor1.teach(university1, mathematics);
professor1.teach(university2, mathematics);
associateProfessor2.teach(university1, databases);
associateProfessor2.teach(university1, philosophy);
associateProfessor2.teach(university5, philosophy);

const administrator1 = new Administrator(
    300,
    "Mr. Yellow",
    37,
    "male",
    "football",
    8,
    university2
);
university2.addEmployee(administrator1);
const administrator2 = new Administrator(
    301,
    "Hanna Snow",
    47,
    "female",
    "football",
    12,
    null
);
const administrator3 = new Administrator(
    302,
    "Georgina Belotti",
    23,
    "female",
    "football",
    3,
    null
);
administrator3.applyForJob(university1);
administrator3.salary = administrator3.calculateSalary();

administrator1.makeNote<string>("Buy 2kg apples");
administrator1.makeNote<number>(42000);
administrator1.makeNote<string[]>(["apple", "banana", "orange"]);

administrator1.applyForLeave();
administrator2.applyForLeave();
rector.applyForLeave();
professor1.applyForLeave(university1);
professor1.applyForLeave(university3);

const student1 = new Student(
    10,
    "John Black",
    16,
    "male",
    "swimming",
    Major.ComputerScience,
    YearOfStudy.First,
    4.9,
    null
);
const student2 = new Student(
    11,
    "Sarah White",
    21,
    "female",
    "reading",
    Major.Cybersecurity,
    YearOfStudy.First,
    3.8,
    null
);
const student3 = new Student(
    12,
    "Tom Green",
    15,
    "male",
    "running",
    Major.SoftwareEngineering,
    YearOfStudy.First,
    4.2,
    null
);
const student4 = new Student(
    13,
    "Alice Red",
    22,
    "female",
    "painting",
    Major.ArtificialIntelligence,
    YearOfStudy.Fifth,
    4.1,
    null
);
const student5 = new Student(
    14,
    "Bob Yellow",
    20,
    "male",
    "music",
    Major.Linguistics,
    YearOfStudy.Second,
    3.7,
    null
);
const student6 = new Student(
    15,
    "Emily Black",
    21,
    "female",
    "dancing",
    Major.Marketing,
    YearOfStudy.Fifth,
    4.3,
    null
);
const student7 = new Student(
    16,
    "Mark Orange",
    19,
    "male",
    "gaming",
    Major.ComputerScience,
    YearOfStudy.First,
    4.6,
    null
);
const student8 = new Student(
    17,
    "Alexander White",
    19,
    "male",
    "reading",
    Major.ComputerScience,
    YearOfStudy.First,
    4.6,
    university1
);

university1.addStudent(student8);
student1.applyToUniversity(university1);
student2.applyToUniversity(university3);
student3.applyToUniversity(university1);
student4.applyToUniversity(university4);
student5.applyToUniversity(university5);
student6.applyToUniversity(university5);
student7.applyToUniversity(university1);

student1.attendLectures();
student4.attendLectures();

student4.leaveStudying();

rector.introduce();
student2.introduce();

associateProfessor1.participateInConference("Robotics");

professor1.publishScientificArticle(
    "Artificial Intelligence",
    "The rapid advancement of artificial intelligence (AI) and machine learning has revolutionized various industries and reshaped our society."
);

professor1.mentorAssociateProfessor(associateProfessor1);

console.log(rector.distributeBudget(115000));

const employee1: Employee = {
    id: 1,
    name: "John",
    age: 30,
    gender: "male",
    hobby: "reading",
    role: EmployeeRole.Administrator,
    experience: 5,
    salary: 5000,
    university: null
};

const musicElective = new ElectiveCourse<TeachingEmployee>(
    CourseName.Music,
    true,
    "7 weeks"
);
const virtualRealityElective = new ElectiveCourse<TeachingEmployee>(
    CourseName.VirtualReality,
    true
);

university1.addCourse(virtualRealityElective);
professor3.addCourseToTeaching(university1, virtualRealityElective);

const ticket1 = new Ticket(
    500,
    TicketType.EquipmentBrokeDown,
    "Computer in room 567 does not turn on"
);
const ticket2 = new Ticket(
    501,
    TicketType.RanOutOfMaterials,
    "Ran out of pens"
);
const ticket3 = new Ticket(
    502,
    TicketType.Dirty,
    "Dirty floor in auditorium 123"
);

administrator3.addTicket(ticket1);
administrator3.addTicket(ticket2);
administrator3.addTicket(ticket3);

administrator3.handleTicket(501);
console.log(administrator3.tickets);
administrator3.handleTicket(64746);

const university1Year = university1.getPropertyByName("foundationYear");
console.log(university1Year);

console.log("Harvard University students:", university1.students);

administrator3.salary = 7000;

const maxSalaryEmployee = university1.getMaxSalaryEmployee();
console.log(maxSalaryEmployee);

console.log(university1.filterStudentsByHobbyReading());

university1.getTeachersByCourse(mathematics);

console.log(university1.filterEmployeesByAgeOver50());

console.log(university1.filterEmployeesByExperienceLess5());

console.log(registeredClasses);

console.log(mathematics);

//university1.deleteAllStudents();
