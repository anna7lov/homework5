import { EmployeeRole } from "../enums/enums";
import { IPerson, IUniversity } from "../interfaces/interfaces";

export type Gender = "male" | "female";

export type Employee = IPerson & {
    role: EmployeeRole;
    experience: number;
    salary: number;
} & ({ universities: Set<IUniversity> } | { university: IUniversity | null });

export type PersonWithAge = Pick<IPerson, "name" | "age">;

export type PersonWithHobby = Omit<IPerson, "id" | "age" | "gender">;
