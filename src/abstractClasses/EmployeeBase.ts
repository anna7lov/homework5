import { Person } from "../classes/Person";
import { EmployeeRole } from "../enums/enums";
import { IUniversity } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { AgeValidator, Log } from "../decorators/decorators";

export abstract class EmployeeBase extends Person {
    private _salary: number;

    constructor(
        public id: number,
        public name: string,
        public age: number,
        public gender: Gender,
        public hobby: string,
        public role: EmployeeRole,
        public experience: number
    ) {
        super(id, name, age, gender, hobby);
        this._salary = 0;
    }

    get salary(): number {
        return this._salary;
    }

    set salary(value: number) {
        this._salary = value;
    }

    abstract calculateSalary(): number;

    @AgeValidator(17)
    @Log("has applied for a job at")
    applyForJob(university: IUniversity): void {
        university.considerEmployee(this);
    }
}
