import { EmployeeBase } from "./EmployeeBase";
import { EmployeeRole } from "../enums/enums";
import { IUniversity } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { checkAccess } from "../decorators/decorators";

export abstract class NonTeachingEmployee extends EmployeeBase {
    private _university: IUniversity | null;

    constructor(
        id: number,
        name: string,
        age: number,
        gender: Gender,
        hobby: string,
        role: EmployeeRole,
        experience: number,
        _university: IUniversity | null
    ) {
        super(id, name, age, gender, hobby, role, experience);
        this._university = _university;
    }

    @checkAccess
    applyForLeave(): void {
        this.university?.considerLeave(this);
    }

    get university(): IUniversity | null {
        return this._university;
    }

    set university(value: IUniversity | null) {
        this._university = value;
    }
}
