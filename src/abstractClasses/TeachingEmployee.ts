import { EmployeeBase } from "./EmployeeBase";
import { EmployeeRole } from "../enums/enums";
import {
    IUniversity,
    ICourse,
    IElectiveCourse
} from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { checkAccess, Log } from "../decorators/decorators";

export abstract class TeachingEmployee extends EmployeeBase {
    protected _universities: Set<IUniversity>;
    protected teachingAssignments: Map<IUniversity, ICourse[]>;

    constructor(
        id: number,
        name: string,
        age: number,
        gender: Gender,
        hobby: string,
        role: EmployeeRole,
        experience: number,
        universities: Set<IUniversity>
    ) {
        super(id, name, age, gender, hobby, role, experience);
        this._universities = universities;
        this.teachingAssignments = new Map();
    }

    @checkAccess
    @Log("has added course to teaching at")
    addCourseToTeaching(
        university: IUniversity,
        course: ICourse | IElectiveCourse
    ): void {
        university.considerTeachingCourse(this, course);
    }

    @checkAccess
    teach(university: IUniversity, course: ICourse): void {
        if (this.teachingAssignments.has(university)) {
            const assignedCourses = this.teachingAssignments.get(university);
            if (assignedCourses?.includes(course)) {
                console.log(
                    `${this.name} gave a lecture on the course ${course.name} at ${university.name}`
                );
            } else {
                console.log(
                    `${this.name} is not assigned to teach ${course.name} at ${university.name}`
                );
            }
        } else {
            console.log(
                `${this.name} is not assigned to teach at ${university.name}`
            );
        }
    }

    @checkAccess
    applyForLeave(university: IUniversity): void {
        university.considerLeave(this);
    }

    getAssignmentsListByUniversity(
        university: IUniversity
    ): ICourse[] | undefined {
        return this.teachingAssignments.get(university);
    }

    get universities(): Set<IUniversity> {
        return this._universities;
    }

    set universities(value: Set<IUniversity>) {
        this._universities = value;
    }

    get assignments(): Map<IUniversity, ICourse[]> {
        return this.teachingAssignments;
    }
}
