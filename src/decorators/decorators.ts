import "reflect-metadata";
import { NonTeachingEmployee } from "../abstractClasses/NonTeachingEmployee";
import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { Person } from "../classes/Person";
import { Student } from "../classes/Student";

export function Log(message: string): MethodDecorator {
    return function (
        target: Object,
        propertyKey: string | symbol,
        descriptor: PropertyDescriptor
    ): void {
        const originalMethod = descriptor.value;

        descriptor.value = function (...args: any[]): any {
            const name = (this as Person).name;
            console.log(`${name} ${message} ${args[0]?.name ? args[0].name : ""}`);
            return originalMethod.apply(this, args);
        };
    };
}

export function AgeValidator(minAge: number): MethodDecorator {
    return function (
        target: Object,
        propertyKey: string | symbol,
        descriptor: PropertyDescriptor
    ): void {
        const originalMethod = descriptor.value;

        descriptor.value = function (...args: any[]): any {
            const person = this as Person;
            if (person.age < minAge) {
                console.log(`Invalid age. Age must be at least ${minAge} years.`);
                return;
            }
            return originalMethod.apply(this, args);
        };
    };
}

export function checkAccess(
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
): void {
    const originalMethod = descriptor.value;

    descriptor.value = function (...args: any[]): any {
        const university = (this as NonTeachingEmployee | Student).university;
        const universities = (this as TeachingEmployee).universities;
        if (
            ("university" in this && university === null) ||
            ("universities" in this && universities.size === 0)
        ) {
            console.log(
                "Person does not belong to any university now. Method cannot be executed"
            );
            return;
        }
        return originalMethod.apply(this, args);
    };
}

export function LogGetter(
    target: Object,
    propertyKey: string | symbol,
    descriptor: PropertyDescriptor
): void {
    const originalGetter = descriptor.get;

    descriptor.get = function (this: any): any {
        console.log(`Accessing getter: ${propertyKey.toString()}`);
        return originalGetter?.apply(this);
    };
}

export const registeredClasses: (new (...args: any[]) => {})[] = [];
export function Register<T extends { new(...args: any[]): {} }>(target: T) {
    registeredClasses.push(target);
}

const formatMetadataKey = Symbol("format");
export function format(formatString: string) {
    return Reflect.metadata(formatMetadataKey, formatString);
}
export function getFormat(target: any, propertyKey: string) {
    return Reflect.getMetadata(formatMetadataKey, target, propertyKey);
}
