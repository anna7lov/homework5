export enum EmployeeRole {
    Rector = "RECTOR",
    Professor = "PROFESSOR",
    AssociateProfessor = "ASSOCIATE_PROFESSOR",
    Administrator = "ADMINISTRATOR"
}

export enum AccreditationStatus {
    Accredited = "ACCREDITED",
    NotAccredited = "NOT_ACCREDITED",
    InProcess = "IN_PROCESS"
}

export enum CourseName {
    Mathematics = "MATHEMATICS",
    ComputerScience = "COMPUTER_SCIENCE",
    Databases = "DATABASES",
    Literature = "LITERATURE",
    History = "HISTORY",
    Philosophy = "PHILOSOPHY",
    Psychology = "PSYCHOLOGY",
    English = "ENGLISH",
    Economics = "ECONOMICS",
    Marketing = "MARKETING",
    Finance = "FINANCE",
    Music = "MUSIC",
    VirtualReality = "VIRTUAL_REALITY"
}

export enum Major {
    ComputerScience = "Computer Science",
    DataScience = "Data Science",
    ArtificialIntelligence = "Artificial Intelligence",
    SoftwareEngineering = "Software Engineering",
    Cybersecurity = "Cybersecurity",
    Marketing = "Marketing",
    Linguistics = "Linguistics"
}

export enum YearOfStudy {
    First = 1,
    Second = 2,
    Third = 3,
    Fourth = 4,
    Fifth = 5
}

export enum TicketType {
    EquipmentBrokeDown = "EQUIPMENT BROKE DOWN",
    RanOutOfMaterials = "RAN OUT OF MATERIALS",
    Dirty = "DIRTY",
    Other = "OTHER"
}
