import { NonTeachingEmployee } from "../abstractClasses/NonTeachingEmployee";
import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";

export function isTeachingEmployee(
    employee: NonTeachingEmployee | TeachingEmployee
): employee is TeachingEmployee {
    return (employee as TeachingEmployee).assignments !== undefined;
}
