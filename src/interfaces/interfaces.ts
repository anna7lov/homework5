import { EmployeeBase } from "../abstractClasses/EmployeeBase";
import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { Major, YearOfStudy, CourseName, TicketType } from "../enums/enums";
import { Gender } from "../types/types";

export interface IPerson {
    id: number;
    name: string;
    age: number;
    gender: Gender;
    hobby: string;
}

export interface IStudent extends IPerson {
    university: IUniversity | null;
    major: Major;
    year: YearOfStudy;
    gradePointAverage: number;
}

export interface IUniversity {
    name: string;
    foundationYear: number;
    location: string;
    considerEmployee(employee: EmployeeBase): void;
    considerStudent(student: IStudent): void;
    considerTeachingCourse(teacher: TeachingEmployee, course: ICourse): void;
    considerLeave(employee: EmployeeBase): void;
    expellStudent(student: IStudent): void;
}

export interface ICourse {
    name: CourseName;
    addTeacher(teacher: TeachingEmployee): void;
}

export interface IElectiveCourse extends ICourse {
    optional: boolean;
    duration?: string;
}

export interface BudgetDistribution {
    salaries: number;
    research: number;
    educationalMaterials: number;
    equipment: number;
    otherExpenses: number;
}

export interface ScientificArticle {
    title: string;
    body: string;
}

export interface ITicket {
    id: number;
    type: TicketType;
    description: string;
}
