import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { AssociateProfessor } from "./AssociateProfessor";
import { EmployeeRole } from "../enums/enums";
import { IUniversity, ScientificArticle } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { Register, checkAccess } from "../decorators/decorators";

@Register
export class Professor extends TeachingEmployee {
    constructor(
        id: number,
        name: string,
        age: number,
        gender: Gender,
        hobby: string,
        experience: number,
        _universities: Set<IUniversity>
    ) {
        super(
            id,
            name,
            age,
            gender,
            hobby,
            EmployeeRole.Professor,
            experience,
            _universities
        );
    }

    @checkAccess
    calculateSalary(): number {
        if (this.experience > 7 && this.teachingAssignments.size > 2) {
            return 4500;
        } else if (this.teachingAssignments.size > 2) {
            return 4000;
        } else if (this.experience > 5 && this.teachingAssignments.size > 1) {
            return 3200;
        } else {
            return 2500;
        }
    }

    publishScientificArticle(topic: string, body: string): ScientificArticle {
        const article: ScientificArticle = {
            title: topic,
            body: body
        };

        return article;
    }

    mentorAssociateProfessor(associateProfessor: AssociateProfessor): void {
        console.log(
            `Professor ${this.name} gave recommendations to associate professor ${associateProfessor.name}.`
        );
    }
}
