import { Person } from "./Person";
import { Major, YearOfStudy } from "../enums/enums";
import { IStudent, IUniversity } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import {
    Register,
    AgeValidator,
    Log,
    checkAccess
} from "../decorators/decorators";

@Register
export class Student extends Person implements IStudent {
    constructor(
        public id: number,
        public name: string,
        public age: number,
        public gender: Gender,
        public hobby: string,
        public major: Major,
        public year: YearOfStudy,
        public gradePointAverage: number,
        public university: IUniversity | null
    ) {
        super(id, name, age, gender, hobby);
    }

    @AgeValidator(17)
    @Log("has applied to")
    applyToUniversity(university: IUniversity): void {
        university.considerStudent(this);
    }

    @checkAccess
    attendLectures(): void {
        console.log(`Student ${this.name} has attended lectures`);
    }

    @checkAccess
    @Log("has applied to leave")
    leaveStudying(): void {
        this.university?.expellStudent(this);
    }
}
