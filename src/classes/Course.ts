import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { CourseName } from "../enums/enums";
import { ICourse } from "../interfaces/interfaces";
import { Register, LogGetter } from "../decorators/decorators";

@Register
export class Course<T extends TeachingEmployee> implements ICourse {
    private _teachers: Set<T> = new Set();

    constructor(public name: CourseName) { }

    addTeacher(teacher: T): void {
        if (!this._teachers.has(teacher)) {
            this._teachers.add(teacher);
            console.log(
                `${teacher.name} has been added to the list of teachers teaching the ${this.name} course`
            );
        }
    }

    @LogGetter
    get teachers(): ReadonlySet<T> {
        return this._teachers;
    }
}
