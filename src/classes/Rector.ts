import { NonTeachingEmployee } from "../abstractClasses/NonTeachingEmployee";
import { EmployeeRole } from "../enums/enums";
import { IUniversity, BudgetDistribution } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { Register, checkAccess } from "../decorators/decorators";

@Register
export class Rector extends NonTeachingEmployee {
    constructor(
        id: number,
        name: string,
        age: number,
        gender: Gender,
        hobby: string,
        experience: number,
        _university: IUniversity | null
    ) {
        super(
            id,
            name,
            age,
            gender,
            hobby,
            EmployeeRole.Rector,
            experience,
            _university
        );
    }

    @checkAccess
    calculateSalary(): number {
        if (this.experience > 5) {
            return 4000;
        } else {
            return 3500;
        }
    }

    @checkAccess
    distributeBudget(amount: number): BudgetDistribution {
        let salaries;
        let research;
        let educationalMaterials;
        let equipment;
        let otherExpenses;

        if (amount > 100000) {
            salaries = amount * 0.4;
            research = amount * 0.15;
            educationalMaterials = amount * 0.1;
            equipment = amount * 0.12;
            otherExpenses =
                amount - (salaries + research + educationalMaterials + equipment);
        } else {
            salaries = amount * 0.5;
            research = amount * 0.08;
            educationalMaterials = amount * 0.13;
            equipment = amount * 0.1;
            otherExpenses =
                amount - (salaries + research + educationalMaterials + equipment);
        }

        const budgetDistribution: BudgetDistribution = {
            salaries,
            research,
            educationalMaterials,
            equipment,
            otherExpenses
        };

        return budgetDistribution;
    }

    @checkAccess
    representUniversity(): void {
        console.log(
            `Rector ${this.name} is representing ${this.university?.name} at external events.`
        );
    }
}
