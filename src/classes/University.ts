import { EmployeeBase } from "../abstractClasses/EmployeeBase";
import { NonTeachingEmployee } from "../abstractClasses/NonTeachingEmployee";
import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { Administrator } from "./Administrator";
import { AssociateProfessor } from "./AssociateProfessor";
import { Professor } from "./Professor";
import { Rector } from "./Rector";
import { isTeachingEmployee } from "../utils/isTeachingEmployee";
import { AccreditationStatus } from "../enums/enums";
import {
    IUniversity,
    ICourse,
    IElectiveCourse,
    IStudent
} from "../interfaces/interfaces";
import { Employee, PersonWithAge, PersonWithHobby } from "../types/types";
import {
    Register,
    Log,
    LogGetter,
    format,
    getFormat
} from "../decorators/decorators";

@Register
export class University implements IUniversity {
    @format("toUpperCase")
    public location: string;
    private _employees: Set<Employee> = new Set();
    private _students: Set<IStudent> = new Set();
    public courses: Set<ICourse | IElectiveCourse> = new Set();

    constructor(
        public name: string,
        public readonly foundationYear: number,
        location: string,
        public accreditationStatus: AccreditationStatus
    ) {
        this.location = location;
    }

    getFormatedLocation(): string {
        let formatString = getFormat(this, "location");
        const method = `this.location.${formatString}()`;
        const result = eval(method);
        return result;
    }

    addCourse(course: ICourse | IElectiveCourse): void {
        if (!this.courses.has(course)) {
            this.courses.add(course);
            console.log(`Course ${course.name} has been added to ${this.name}`);
        }
    }

    addStudent(student: IStudent): void {
        this._students.add(student);
    }

    addEmployee(employee: Employee): void {
        this._employees.add(employee);
    }

    @Log("has considered employee")
    considerEmployee(employee: NonTeachingEmployee | TeachingEmployee): void {
        if (this._employees.has(employee)) {
            console.log(`The employee is already working at this university`);
        } else if (isTeachingEmployee(employee) && employee.experience > 2) {
            console.log(`${employee.name} is teaching employee`);
            employee.universities.add(this);
            this._employees.add(employee);
            console.log(
                `Employee teacher ${employee.name} has been hired in ${this.name}`
            );
        } else if (!isTeachingEmployee(employee) && employee.experience > 1) {
            console.log(`${employee.name} is not teaching employee`);
            employee.university = this;
            this._employees.add(employee);
            console.log(`Employee ${employee.name} has been hired in ${this.name}`);
        } else {
            console.log(`Employee ${employee.name} can not be hired`);
        }
    }

    considerTeachingCourse(teacher: TeachingEmployee, course: ICourse): void {
        if (!this._employees.has(teacher)) {
            console.log(`${teacher.name} does not work at this university now`);
            return;
        } else if (!this.courses.has(course)) {
            console.log(`The course ${course.name} does not exist in the university`);
            return;
        }
        const assignments = teacher.getAssignmentsListByUniversity(this);
        if (!teacher.assignments.has(this)) {
            teacher.assignments.set(this, []);
        }
        if (!teacher.assignments.get(this)?.includes(course)) {
            teacher.assignments.get(this)?.push(course);
            console.log(
                `${teacher.name} started teaching ${course.name} course at ${this.name}`
            );
            course.addTeacher(teacher);
        } else {
            console.log(
                `Course ${course.name} is already being taught by ${teacher.name} at ${this.name}`
            );
        }
    }

    @Log("has considered leave")
    considerLeave(employee: EmployeeBase): void {
        if (employee instanceof Rector) {
            console.log("The vacation will be granted for 3 weeks.");
        } else if (employee instanceof Administrator) {
            console.log("The vacation will be granted for 1 week.");
        } else if (employee instanceof Professor && this._employees.has(employee)) {
            console.log("The vacation will be granted for 2 weeks.");
        } else if (
            employee instanceof AssociateProfessor &&
            this._employees.has(employee)
        ) {
            console.log("The vacation will be granted for 1.5 week.");
        } else {
            console.log("The vacation denied");
        }
    }

    @Log("has considered student")
    considerStudent(student: IStudent): void {
        if (
            (student.university === null &&
                this.name === "Harvard University" &&
                student.gradePointAverage > 4.5) ||
            (student.university === null &&
                this.name === "Stanford University" &&
                student.gradePointAverage > 4.4) ||
            (student.university === null &&
                this.name === "University of Cambridge" &&
                student.gradePointAverage > 4.2) ||
            (student.university === null &&
                this.name !== "Harvard University" &&
                this.name !== "Stanford University" &&
                this.name !== "University of Cambridge" &&
                student.gradePointAverage > 3.8)
        ) {
            this.addStudent(student);
            student.university = this;
            console.log(
                `Student ${student.name} with GPA ${student.gradePointAverage} has been admitted in ${this.name}`
            );
        } else if (student.university !== null) {
            console.log(
                `Student ${student.name} must first leave the current university`
            );
        } else {
            console.log(
                `Student ${student.name} with GPA ${student.gradePointAverage} can not be admitted`
            );
        }
    }

    expellStudent(student: IStudent): void {
        if (this._students.has(student)) {
            this._students.delete(student);
            student.university = null;
            console.log(`Student ${student.name} has been removed from ${this.name}`);
        } else {
            console.log(`Student ${student.name} does not belong to ${this.name}`);
        }
    }

    filterEmployeesByAgeOver50(): PersonWithAge[] {
        return Array.from(this._employees)
            .filter((employee) => employee.age > 50)
            .map(({ name, age }) => ({ name, age }));
    }

    filterEmployeesByExperienceLess5(): [string, number][] {
        return Array.from(this._employees)
            .filter((employee) => employee.experience < 5)
            .map(({ name, experience }) => [name, experience]);
    }

    filterStudentsByHobbyReading(): PersonWithHobby[] {
        return Array.from(this._students)
            .filter((student) => student.hobby === "reading")
            .map(({ name, hobby }) => ({ name, hobby }));
    }

    getTeachersByCourse(course: ICourse): string[] | null {
        const teachers = Array.from(this._employees)
            .filter((employee) => {
                if (employee instanceof TeachingEmployee) {
                    const teachingAssignments = employee.getAssignmentsListByUniversity(
                        this
                    );
                    return teachingAssignments?.some(
                        (assignedCourse) => assignedCourse.name === course.name
                    );
                }
                return false;
            })
            .map((teacher) => teacher.name);

        if (teachers.length > 0) {
            console.log(
                `Teachers who teach the course ${course.name} at
                ${this.name}:\n${teachers.join("\n")}`
            );
            return teachers;
        } else {
            console.log("Nothing to display");
            return null;
        }
    }

    getMaxSalaryEmployee(): Employee | null {
        const employees = Array.from(this._employees);
        if (employees.length === 0) {
            return null;
        }
        return employees.reduce((maxSalaryEmployee, currentEmployee) => {
            return currentEmployee.salary > maxSalaryEmployee.salary
                ? currentEmployee
                : maxSalaryEmployee;
        });
    }

    getPropertyByName<K extends keyof IUniversity>(
        propertyName: K
    ): IUniversity[K] {
        return this[propertyName];
    }

    @LogGetter
    get employees(): ReadonlySet<Employee> {
        return this._employees;
    }

    @LogGetter
    get students(): ReadonlySet<IStudent> {
        return this._students;
    }

    deleteAllStudents(): never {
        throw new Error("Deleting all students is not allowed.");
    }
}
