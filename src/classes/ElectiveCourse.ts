import { Course } from "./Course";
import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { CourseName } from "../enums/enums";
import { IElectiveCourse } from "../interfaces/interfaces";
import { Register } from "../decorators/decorators";

@Register
export class ElectiveCourse<T extends TeachingEmployee> extends Course<T>
    implements IElectiveCourse {
    constructor(
        public name: CourseName,
        public optional: boolean,
        public duration?: string
    ) {
        super(name);
    }
}
