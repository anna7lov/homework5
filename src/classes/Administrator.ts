import { NonTeachingEmployee } from "../abstractClasses/NonTeachingEmployee";
import { EmployeeRole } from "../enums/enums";
import { ITicket, IUniversity } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { Register, checkAccess } from "../decorators/decorators";

@Register
export class Administrator extends NonTeachingEmployee {
    private _tickets: ITicket[] = [];

    constructor(
        id: number,
        name: string,
        age: number,
        gender: Gender,
        hobby: string,
        experience: number,
        _university: IUniversity | null
    ) {
        super(
            id,
            name,
            age,
            gender,
            hobby,
            EmployeeRole.Administrator,
            experience,
            _university
        );
    }

    @checkAccess
    calculateSalary(): number {
        if (this.experience > 5) {
            return 2000;
        } else {
            return 1700;
        }
    }

    @checkAccess
    addTicket(ticket: ITicket): void {
        this._tickets.push(ticket);
        console.log(`Ticket added: ${ticket.description}`);
    }

    @checkAccess
    handleTicket(id: number): void {
        const ticketIndex = this._tickets.findIndex((ticket) => ticket.id === id);
        if (ticketIndex !== -1) {
            this._tickets = this._tickets.filter((ticket) => ticket.id !== id);
            console.log(`Ticket with id ${id} handled.`);
        } else {
            console.log(`Ticket with id ${id} not found.`);
        }
    }

    makeNote<T>(item: T): void {
        const note = `Note: ${JSON.stringify(item)}`;
        console.log(note);
    }

    get tickets(): ITicket[] {
        return this._tickets;
    }
}
