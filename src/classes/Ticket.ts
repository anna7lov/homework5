import { TicketType } from "../enums/enums";
import { ITicket } from "../interfaces/interfaces";
import { Register } from "../decorators/decorators";

@Register
export class Ticket implements ITicket {
    constructor(
        public id: number,
        public type: TicketType,
        public description: string
    ) { }
}
