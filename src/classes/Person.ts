import { IPerson } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { Register } from "../decorators/decorators";

@Register
export class Person implements IPerson {
    constructor(
        public id: number,
        public name: string,
        public age: number,
        public gender: Gender,
        public hobby: string
    ) { }

    introduce(): void {
        console.log(
            `${this.name} is ${this.age} years old, ${this.gender}. Hobby: ${this.hobby}.`
        );
    }
}
