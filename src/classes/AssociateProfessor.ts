import { TeachingEmployee } from "../abstractClasses/TeachingEmployee";
import { EmployeeRole } from "../enums/enums";
import { IUniversity } from "../interfaces/interfaces";
import { Gender } from "../types/types";
import { Register, checkAccess } from "../decorators/decorators";

@Register
export class AssociateProfessor extends TeachingEmployee {
    constructor(
        id: number,
        name: string,
        age: number,
        gender: Gender,
        hobby: string,
        experience: number,
        _universities: Set<IUniversity>
    ) {
        super(
            id,
            name,
            age,
            gender,
            hobby,
            EmployeeRole.AssociateProfessor,
            experience,
            _universities
        );
    }

    @checkAccess
    calculateSalary(): number {
        if (this.experience > 4 && this.teachingAssignments.size > 2) {
            return 4000;
        } else if (this.teachingAssignments.size > 2) {
            return 3500;
        } else if (
            (this.experience > 2 && this.teachingAssignments.size > 1) ||
            (this.experience > 3 && this.teachingAssignments.size === 1)
        ) {
            return 2800;
        } else {
            return 2300;
        }
    }

    participateInConference(conferenceName: string): void {
        console.log(
            `Associate Professor ${this.name} took part in a scientific conference ${conferenceName}.`
        );
    }
}
